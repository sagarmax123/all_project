(function ($) {
	"use strict";
	jQuery(document).ready(function ($) {
		$(".client-testimonial-carousel").owlCarousel({
			items: 2
			, margin: 30
			, nav: false
			, dots: true
			, loop: true
			, autoplay: true
		});
	});
	jQuery(window).load(function () {});
	/********** Portfolio/gallery  ********/
	$(document).ready(function () {
		$(".filter-button").click(function () {
			var value = $(this).attr('data-filter');
			if (value == "all") {
				//$('.filter').removeClass('hidden');
				$('.filter').show('1000');
			}
			else {
				$(".filter").not('.' + value).hide('3000');
				$('.filter').filter('.' + value).show('3000');
			}
		});
	});
	/*Skills */
	$(document).ready(function () {
		$('.html').css('width', '90%');
		$('.css').css('width', '90%');
		$('.jquery').css('width', '70%');
		$('.javascript').css('width', '60%');
		$('.adobe').css('width', '80%');
		$('.wordpress').css('width', '10%');
		$('.ui').css('width', '70%');
		$('.rwd').css('width', '80%');
	});
	// Back to top button
	(function () {
		$(document).ready(function () {
			return $(window).scroll(function () {
				return $(window).scrollTop() > 200 ? $("#back-to-top").addClass("show") : $("#back-to-top").removeClass("show")
			}), $("#back-to-top").click(function () {
				return $("html,body").animate({
					scrollTop: "0"
				})
			})
		})
	}).call(this);
	/*Menu sticky*/
	$(document).ready(function () {
		var x = $('#nav').offset().top;
		$(window).scroll(function () {
			var y = $(window).scrollTop();
			if (y > x) {
				$('#nav').addClass('sticyMenu');
			}
			else {
				$('#nav').removeClass('sticyMenu');
			}
		});
		$('#nav').wrapAll('<div class="baba">');
		$('.baba').css('height', $('#nav').outerHeight());
	});
}(jQuery));

/*Contact Form*/

$(document).ready(function() {
	// Test for placeholder support
    $.support.placeholder = (function(){
        var i = document.createElement('input');
        return 'placeholder' in i;
    })();

    // Hide labels by default if placeholders are supported
    if($.support.placeholder) {
        $('.form-label').each(function(){
            $(this).addClass('js-hide-label');
        });  

        // Code for adding/removing classes here
        $('.form-group').find('input, textarea').on('keyup blur focus', function(e){
            
            // Cache our selectors
            var $this = $(this),
                $parent = $this.parent().find("label");

            if (e.type == 'keyup') {
                if( $this.val() == '' ) {
                    $parent.addClass('js-hide-label'); 
                } else {
                    $parent.removeClass('js-hide-label');   
                }                     
            } 
            else if (e.type == 'blur') {
                if( $this.val() == '' ) {
                    $parent.addClass('js-hide-label');
                } 
                else {
                    $parent.removeClass('js-hide-label').addClass('js-unhighlight-label');
                }
            } 
            else if (e.type == 'focus') {
                if( $this.val() !== '' ) {
                    $parent.removeClass('js-unhighlight-label');
                }
            }
        });
    } 
});